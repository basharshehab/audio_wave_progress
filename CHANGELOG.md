## 0.0.44

* Still fixing the package [NOT WORKING YET]
* Code refactoring 

## 0.0.43

* Still fixing the package [NOT WORKING YET]
* Used flip_card library works great.
* added option to customize padding between buttons.

## 0.0.40

* Still fixing the package [NOT WORKING YET]
* Used flip_card library to do play button [testing]

## 0.0.29

* Still fixing the package [NOT WORKING YET]
* changed play/pause icons

## 0.0.29

* Still fixing the package [NOT WORKING YET]
* added green container behind icons to try and avoid the white frame between animations

## 0.0.28

* Still fixing the package [NOT WORKING YET]
* reversed last update
* trying different animations


## 0.0.27

* Still fixing the package [NOT WORKING YET]
* A bit faster animation 
* testing another play button

## 0.0.26

* Still fixing the package [NOT WORKING YET]
* Shortened animation from play button significantly

## 0.0.25

* Still fixing the package [NOT WORKING YET]
* Re-added padding on the sides 

## 0.0.24

* Still fixing the package [NOT WORKING YET]
* Optimizing padding solution

## 0.0.23

* Still fixing the package [NOT WORKING YET]
* Fixed UI margins

## 0.0.22

* Still fixing the package [NOT WORKING YET]
* Adjusting UI

## 0.0.21

* Still fixing the package [NOT WORKING YET]
* Adjusting UI

## 0.0.20

* Still fixing the package [NOT WORKING YET]
* Adjusting UI

## 0.0.19

* Still fixing the package [NOT WORKING YET]
* Adjusting UI

## 0.0.18

* Still fixing the package [NOT WORKING YET]
* Adjusting UI
* Changing animations 

## 0.0.17

* Still fixing the package [NOT WORKING YET]
* Adjusting UI
* Changing animations 

## 0.0.16

* Still fixing the package [NOT WORKING YET]
* Adjusting UI
* Fixing animations 
## 0.0.15

* Still fixing the package [NOT WORKING YET]
* Adjusting UI
* Fixing animations 

## 0.0.14

* Still fixing the package [NOT WORKING YET]
* Adjusting UI
* Fixing animations 

## 0.0.13

* Still fixing the package [NOT WORKING YET]
* Adjusting UI
* Added animations 

## 0.0.12

* Still fixing the package [NOT WORKING YET]
* Adjusting UI 

## 0.0.11

* Still fixing the package [NOT WORKING YET]
* Adjusting UI 

## 0.0.10

* Still fixing the package [NOT WORKING YET]
* fixed assets paths. 

## 0.0.9

* Still fixing the package [NOT WORKING YET]

## 0.0.8

* Still fixing the package [NOT WORKING YET]