<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

I couldn't find any audio players with a wave effect, so I made one myself. Enjoy!
## Features

Just like any other audio player, but with the wavy progress indicator.

## Getting started

`Dart >=2.17.0` and `Flutter >=2.0.0`


## License

Licensed under the [Apache License, Version 2.0](LICENSE)