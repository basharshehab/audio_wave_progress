library audio_wave_progress;

import 'dart:math';

import 'package:audio_wave_progress/player_controller.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
part 'audio_bar.dart';
part 'audio_player_wave_progress.dart';
