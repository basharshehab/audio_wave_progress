part of audio_wave_progress;

class AudioPlayerWaveProgress extends StatelessWidget {
  // final double progressBarHeight;
  final List<double> listOfHeights;
  final Color playedColor;
  final Color unplayedColor;
  final double progressBarWidth;

  final Color enabledButtonsColor;
  final Color grayedButtonsColor;
  final double? spacebetweenProgressAndPlayer;
  final EdgeInsetsGeometry? paddingBetweenButtons;
  const AudioPlayerWaveProgress({
    Key? key,
    required this.listOfHeights,
    required this.playedColor,
    required this.unplayedColor,
    required this.progressBarWidth,
    required this.enabledButtonsColor,
    required this.grayedButtonsColor,
    this.paddingBetweenButtons,
    this.spacebetweenProgressAndPlayer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => PlayerController());
    PlayerController controller = Get.find<PlayerController>();
    return Align(
      alignment: Alignment.bottomCenter,
      child: AnimatedBuilder(
        animation: controller,
        builder: (context, child) => Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              onHorizontalDragUpdate: (details) {
                controller.dragProgressTo(details.delta);
                debugPrint(details.delta.toString());
              },
              child: SizedBox(
                height: listOfHeights.reduce(max),
                width: progressBarWidth,
                child: Row(
                  children: controller.generateAudioBars(
                    listOfHeights,
                    playedColor,
                    unplayedColor,
                    progressBarWidth,
                  ),
                ),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.1,
              padding:
                  EdgeInsets.only(top: spacebetweenProgressAndPlayer ?? 26),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GetBuilder<PlayerController>(
                    id: "shuffle",
                    builder: (controller) => InkWell(
                      onTap: controller.toggleShuffle,
                      child: Container(
                        padding:
                            paddingBetweenButtons ?? const EdgeInsets.all(26),
                        alignment: Alignment.center,
                        child: AnimatedSwitcher(
                          duration: const Duration(milliseconds: 400),
                          child: controller.shuffleOn
                              ? SvgPicture.asset(
                                  "packages/audio_wave_progress/assets/shuffle.svg",
                                  color: enabledButtonsColor,
                                  key: const ValueKey("shuffle_on"),
                                )
                              : SvgPicture.asset(
                                  "packages/audio_wave_progress/assets/shuffle.svg",
                                  color: grayedButtonsColor,
                                  key: const ValueKey("shuffle_off"),
                                ),
                        ),
                      ),
                    ),
                  ),
                  GetBuilder<PlayerController>(
                    id: "forward_back",
                    builder: (controller) => InkWell(
                      onTap: controller.forwardBack,
                      child: Container(
                        padding:
                            paddingBetweenButtons ?? const EdgeInsets.all(26),
                        alignment: Alignment.center,
                        // padding: const EdgeInsets.all(10),
                        child: SvgPicture.asset(
                          "packages/audio_wave_progress/assets/back.svg",
                          // package: 'audio_wave_progress',
                          color: grayedButtonsColor,
                        ),
                      ),
                    ),
                  ),
                  // GetBuilder<PlayerController>(
                  //   id: "play",
                  //   builder: (controller) =>
                  InkWell(
                    onTap: controller.togglePlay,
                    child: Container(
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 97, 177, 90),
                        shape: BoxShape.circle,
                      ),
                      height: 75,
                      width: 75,
                      child: FlipCard(
                        key: controller.cardKey,
                        fill: Fill
                            .fillBack, // Fill the back side of the card to make in the same size as the front.
                        direction: FlipDirection.HORIZONTAL, // default
                        speed: 400,
                        // flipOnTouch: true,
                        // onFlip: () {},
                        alignment: Alignment.center,

                        back: const Icon(
                          Icons.pause_rounded,
                          // height: MediaQuery.of(context).size.height * 0.09,
                          // width: MediaQuery.of(context).size.height * 0.09,
                          size: 37.5,
                          color: Color.fromARGB(255, 254, 254, 254),
                          key: ValueKey("playing"),
                        ),
                        front: const Icon(
                          Icons.play_arrow_rounded,
                          // height: MediaQuery.of(context).size.height * 0.09,
                          // width: MediaQuery.of(context).size.height * 0.09,
                          size: 37.5,
                          color: Color.fromARGB(255, 254, 254, 254),
                          key: ValueKey("paused"),
                        ),
                      ),
                      // AnimatedSwitcher(
                      //   duration: const Duration(milliseconds: 400),
                      //   switchInCurve: Curves.easeInToLinear,
                      //   switchOutCurve: Curves.easeInToLinear.flipped,
                      //   child: controller.isPlaying
                      // ? const Icon(
                      //     Icons.pause_rounded,
                      //     // height: MediaQuery.of(context).size.height * 0.09,
                      //     // width: MediaQuery.of(context).size.height * 0.09,
                      //     size: 37.5,
                      //     color: Color.fromARGB(255, 254, 254, 254),
                      //     key: ValueKey("playing"),
                      //   )
                      // : const Icon(
                      //     Icons.play_arrow_rounded,
                      //     // height: MediaQuery.of(context).size.height * 0.09,
                      //     // width: MediaQuery.of(context).size.height * 0.09,
                      //     size: 37.5,
                      //     color: Color.fromARGB(255, 254, 254, 254),
                      //     key: ValueKey("paused"),
                      //   ),
                      // ),
                      // ),
                    ),
                  ),
                  GetBuilder<PlayerController>(
                    id: "forward_ahead",
                    builder: (controller) => InkWell(
                      onTap: controller.forwardAhead,
                      child: Container(
                        alignment: Alignment.center,
                        padding:
                            paddingBetweenButtons ?? const EdgeInsets.all(26),
                        child: SvgPicture.asset(
                          "packages/audio_wave_progress/assets/ahead.svg",
                          // package: 'audio_wave_progress',
                          color: grayedButtonsColor,
                        ),
                      ),
                    ),
                  ),
                  GetBuilder<PlayerController>(
                    id: "repeat",
                    builder: (controller) => InkWell(
                      onTap: controller.toggleRepeat,
                      child: Container(
                        alignment: Alignment.center,
                        padding:
                            paddingBetweenButtons ?? const EdgeInsets.all(26),
                        child: AnimatedSwitcher(
                          duration: const Duration(milliseconds: 400),
                          child: controller.repeatOn
                              ? SvgPicture.asset(
                                  "packages/audio_wave_progress/assets/repeat.svg",
                                  // package: 'audio_wave_progress',
                                  color: enabledButtonsColor,
                                  key: const ValueKey("repeat_on"),
                                )
                              : SvgPicture.asset(
                                  "packages/audio_wave_progress/assets/repeat.svg",
                                  // package: 'audio_wave_progress',
                                  color: grayedButtonsColor,
                                  key: const ValueKey("repeat_off"),
                                ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
