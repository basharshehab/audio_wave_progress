import 'package:audio_wave_progress/audio_wave_progress.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/widgets.dart';
import 'package:get/state_manager.dart';

class PlayerController extends GetxController {
  int currentlyAt = 0;
  bool shuffleOn = false;
  bool repeatOn = false;
  bool isPlaying = false;
  List<AudioBar> _bars = [];
  double _draggingTracker = 0;
  late GlobalKey<FlipCardState> cardKey;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    cardKey = GlobalKey<FlipCardState>();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  void toggleShuffle() {
    shuffleOn = !shuffleOn;
    update(["shuffle"]);
  }

  void toggleRepeat() {
    repeatOn = !repeatOn;
    update(["repeat"]);
  }

  void togglePlay() {
    isPlaying = !isPlaying;
    cardKey.currentState?.toggleCard();
    update(["play"]);
  }

  void forwardAhead() {
    // repeatOn = !repeatOn;
    // update(["repeat"]);
  }
  void forwardBack() {
    // repeatOn = !repeatOn;
    // update(["repeat"]);
  }

  void resetProgressTo(int index) {
    currentlyAt = index;
    update(["audio_bar"]);
  }

  // TODO fix this shit
  void dragProgressTo(Offset offset) {
    _draggingTracker += offset.dx.abs() * 0.15;
    if (_draggingTracker < 1) {
      return;
    }
    int min = currentlyAt - _draggingTracker.toInt();
    int max = currentlyAt + _draggingTracker.toInt();

    if (offset.dx < 0) {
      if (min < 0) min = 0;
      resetProgressTo(min);
    } else {
      if (max > _bars.length) max = _bars.length;
      resetProgressTo(max);
    }
    _draggingTracker = 0;
    // update();
  }

  List<AudioBar> generateAudioBars(List<double> listOfHeights,
      Color playedColor, Color unplayedColor, double playerWidth) {
    _bars = [];
    double barWidth = playerWidth / listOfHeights.length;
    barWidth = barWidth - playerWidth * 0.0075;
    for (int i = 0; i < listOfHeights.length; i++) {
      _bars.add(
        AudioBar(
          index: i,
          height: listOfHeights[i],
          playedColor: playedColor,
          unplayedColor: unplayedColor,
          width: barWidth,
          playerWidth: playerWidth,
        ),
      );
    }
    return _bars;
  }
}
