part of audio_wave_progress;

class AudioBar extends GetWidget<PlayerController> {
  final double height;
  final double width;
  final Color playedColor;
  final Color unplayedColor;
  final int index;
  final double playerWidth;
  const AudioBar({
    Key? key,
    required this.height,
    required this.playedColor,
    required this.unplayedColor,
    required this.index,
    required this.width,
    required this.playerWidth,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => controller.resetProgressTo(index),
      child: GetBuilder<PlayerController>(
        id: "audio_bar",
        builder: (controller) => Container(
          height: height,
          width: width,
          margin: EdgeInsets.only(right: playerWidth * 0.0075),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color:
                controller.currentlyAt >= index ? playedColor : unplayedColor,
          ),
        ),
      ),
    );
  }
}
